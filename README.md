# BLUETOOTH 5.0 Intervehicular Communication

## Since the discovery of radio, wireless communication technologies are changing the way we live and the way we interact with our environment tremendously. Thanks to the discovery of new and better tools like cellular networks, Wi-Fi and Bluetooth; the world today is a more connected place than it has ever been. 
## In the wake of these new approaches to connectivity, this project aims to explore how we can establish connection between vehicles to provide motorist with a safer and more pleasant ride with the help of the newly released Bluetooth 5.0 standard. It�s two main objectives are establishing a bi-directional audio connection and to share on-board diagnostics data; such as current speed, fuel level or engine status between vehicles. 

# Workflow Diagram

![Scheme](demo-chat/images/diagram.jpg)

![Scheme](demo-chat/images/possible.png)

## We used Texas Instruments CC2640R2F LaunchPad to establish Bluetooth 5.0 Connection

![Scheme](demo-chat/images/launchxl.jpg)

# Chat Screen

## Voice Recognition tools are used to convert users' speech to text.

## Incoming texts are displayed in the upper text box.

## Outgoing texts are displayed in the lower text box.

## On-Board Diagnostic Alerts are displayed on top of the screen

![Scheme](demo-chat/images/demo1.png)

# Settings Screen

## Used for emulation of various On-Board Diagnostic Alerts

![Scheme](demo-chat/images/demo2.png)
