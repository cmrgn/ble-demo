//
//  SettingsViewController.swift
//  demo-chat
//
//  Created by Cem Ergin on 19.12.2017.
//  Copyright © 2017 ELEC491. All rights reserved.
//

import UIKit
import CoreBluetooth

class SettingsViewController: UIViewController {

    @IBOutlet weak var speedSlider: UISlider!
    @IBOutlet weak var settingsView: UIView!
    
    var blePeripheral: CBPeripheral!
    var dataService: CBCharacteristic? = nil
    
    func writeData(value: Int) {
        
        print("Starting OBD Data Transmission")
        
        var parameter = NSInteger(1)
        var data = NSData(bytes: &parameter, length: 1)
        self.blePeripheral.writeValue(data as Data, for: dataService!, type: CBCharacteristicWriteType.withResponse)
        
        usleep(1000000)
        
        parameter = NSInteger(value)
        data = NSData(bytes: &parameter, length: 1)
        self.blePeripheral.writeValue(data as Data, for: dataService!, type: CBCharacteristicWriteType.withResponse)
        
        usleep(1000000)
        
        parameter = NSInteger(4)
        data = NSData(bytes: &parameter, length: 1)
        self.blePeripheral.writeValue(data as Data, for: dataService!, type: CBCharacteristicWriteType.withResponse)
        
        print("OBD Transmission Ended")
    }
    
    @IBAction func batteryPressed(_ sender: Any){
        print("Battery Pressed")
        writeData(value: 5)
    }
    
    @IBAction func temperaturePressed(_ sender: Any){
        print("Temperature Pressed")
        writeData(value: 6)
    }
    
    @IBAction func enginePressed(_ sender: Any){
        print("Engine Pressed")
        writeData(value: 7)
    }
    
    @IBAction func fuelPressed(_ sender: Any){
        print("Fuel Pressed")
        writeData(value: 8)
    }
    
    @IBAction func tractionPressed(_ sender: Any){
        print("Traction Pressed")
        writeData(value: 9)
    }
    
    @IBAction func speedPressed(_ sender: Any){
        print("Speed Pressed \(Int(speedSlider.value)) km/h")
        writeData(value: (Int(speedSlider.value) + 10))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.settingsView.layer.cornerRadius = 5

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
