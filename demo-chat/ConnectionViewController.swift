//
//  ConnectionViewController.swift
//  demo-chat
//
//  Created by Cem Ergin on 19.12.2017.
//  Copyright © 2017 ELEC491. All rights reserved.
//

import UIKit
import CoreBluetooth

class ConnectionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var tableView: UITableView!
    
    var bleManager = BLEManager()
    var discovered: [CBPeripheral] = [] //Discovered BLE devices
    var blePeripheral: CBPeripheral!
    var dataService: CBCharacteristic? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Table view
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource=self
        tableView.delegate=self
        
        
        
        //Discover Listener
        NotificationCenter.default.addObserver(self, selector: #selector(self.didDiscover), name: NSNotification.Name(rawValue: "didDiscover"), object: nil)
        
        
        //Characteristics Listener
        NotificationCenter.default.addObserver(self, selector: #selector(self.didDiscoverCharacteristics), name: NSNotification.Name(rawValue: "didDiscoverCharacteristics"), object: nil)
        
        //Connection Listener
        NotificationCenter.default.addObserver(self, selector: #selector(self.didConnect), name: NSNotification.Name(rawValue: "didConnect"), object: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @objc func didConnect(notif: NSNotification){
        print("Connected to Peripheral")
        blePeripheral.discoverServices(nil)
        print( "Discovering Services...")
        performSegue(withIdentifier: "chatSegue", sender: self)
    }
    
    @objc func didDiscover(notif: NSNotification){
        let res = notif.object as! [String:AnyObject]
        let name = res["name"]
        
        if(!discovered.contains(name as! CBPeripheral)){
            discovered.append(name as! CBPeripheral)
        }
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    
    @objc func didDiscoverCharacteristics(notif: NSNotification){
        let chars = notif.object as! [CBCharacteristic]?

        for ch in chars! {
            print(ch.uuid.uuidString)
            if (ch.uuid.uuidString == "FFF4"){ //For observing
                //Subscribe to notif event
                self.blePeripheral.setNotifyValue(true, for: ch)
            }else if(ch.uuid.uuidString == "FFF1"){ //For sending data
                dataService = ch
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dataServiceChar"), object: ch)
            }
        }
    }
    
    
    
    
    
    
    //TABLE VIEW DELEGATE
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.bleManager.centralManager.stopScan() //stop scanning
        self.blePeripheral = discovered[indexPath.row]
        blePeripheral.delegate = bleManager.bleHandler
        bleManager.centralManager.connect(blePeripheral, options: nil)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return discovered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        //cell.textLabel?.text = discovered[indexPath.row].identifier.uuidString
        cell.textLabel?.text = discovered[indexPath.row].name
        cell.backgroundColor = UIColor.gray
        cell.textLabel?.textColor = UIColor.white
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        return cell
    }
    
    
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "chatSegue") {
            let vc = segue.destination as! ChatViewController
            vc.blePeripheral = blePeripheral
            //vc.dataService = dataService
        }
    }
}
