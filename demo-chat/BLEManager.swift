//
//  BLEManager.swift
//  demo-chat
//
//  Created by Cem Ergin on 19.12.2017.
//  Copyright © 2017 ELEC491. All rights reserved.
//

import Foundation
import CoreBluetooth

class BLEManager {
    var centralManager:CBCentralManager!
    var bleHandler : BLEHandler //delegate
    
    init(){
        self.bleHandler = BLEHandler()
        self.centralManager = CBCentralManager(delegate: self.bleHandler, queue: nil)
}

}
