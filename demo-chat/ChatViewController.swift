//
//  ChatViewController.swift
//  demo-chat
//
//  Created by Cem Ergin on 19.12.2017.
//  Copyright © 2017 ELEC491. All rights reserved.
//

import UIKit
import Speech
import QuartzCore
import CoreBluetooth
import AVFoundation

class ChatViewController: UIViewController, SFSpeechRecognizerDelegate, AVSpeechSynthesizerDelegate {

    var blePeripheral: CBPeripheral!
    var dataService: CBCharacteristic? = nil
    var incomingChar: CBCharacteristic? = nil
    
    enum errorState : Int {
        case initial
        case battery
        case temperature
        case engine
        case fuel
        case traction
        case speed }
    
    var state = errorState.initial
    var mySpeed = 150
    
    @IBOutlet weak var rxtextView: UITextView!
    @IBOutlet weak var txtextView: UITextView!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var repeatButton: UIButton!
    @IBOutlet weak var alertTextButton: UILabel!
    @IBOutlet weak var alertButton: UIImageView!
    @IBOutlet weak var northView: UIView!
    @IBOutlet weak var southView: UIView!
    
    
     func writeData(value: Int) {
        var parameter = NSInteger(value)
        let data = NSData(bytes: &parameter, length: 1)
        self.blePeripheral.writeValue(data as Data, for: dataService!, type: CBCharacteristicWriteType.withResponse)
     }
    
    let speechSynthesizer = AVSpeechSynthesizer()
    var sending = false
    var receiving = false
    
    func sendText(value: String) {
        //Sends Start Line
        
        var parameter = NSInteger(2)
        var data = NSData(bytes: &parameter, length: 1)
        self.blePeripheral.writeValue(data as Data, for: dataService!, type: CBCharacteristicWriteType.withResponse)
        usleep(1000000)
        //Sends Characters
        
        for unit in value.utf8 {
            parameter = NSInteger(unit)
            data = NSData(bytes: &parameter, length: 1)
            self.blePeripheral.writeValue(data as Data, for: dataService!, type: CBCharacteristicWriteType.withResponse)
            //print(unit)
            usleep(1000000)
        }
        //Sends End Line
        parameter = NSInteger(3)
        data = NSData(bytes: &parameter, length: 1)
        self.blePeripheral.writeValue(data as Data, for: dataService!, type: CBCharacteristicWriteType.withResponse)
    }
    
    @IBAction func send(_ sender: Any) {
        sendText(value: txtextView.text)
    }
    
    @IBAction func speakText(_ sender: Any) {
        //playButton.setBackgroundImage(UIImage(named: "play1"), for: .normal)
        //pauseButton.setBackgroundImage(UIImage(named: "pause0"), for: .normal)
        //stopButton.setBackgroundImage(UIImage(named: "stop0"), for: .normal)
        let speechUtterance = AVSpeechUtterance(string: rxtextView.text)
        speechUtterance.voice  = AVSpeechSynthesisVoice(language: "en-GB")
        speechSynthesizer.speak(speechUtterance)
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayback)
            try audioSession.setActive(false, with: .notifyOthersOnDeactivation)
        } catch {
            print("Audio Session Error")
        }
    }
    
    @IBAction func repeatText(_ sender: Any) {
        //repeatButton.setBackgroundImage(UIImage(named: "repeat1"), for: .normal)
        
        let speechUtterance = AVSpeechUtterance(string: txtextView.text)
        speechUtterance.voice  = AVSpeechSynthesisVoice(language: "en-GB")
        speechSynthesizer.speak(speechUtterance)
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayback)
            try audioSession.setActive(false, with: .notifyOthersOnDeactivation)
        } catch {
            print("Audio Session Error")
        }
        
        //repeatButton.setBackgroundImage(UIImage(named: "repeat2"), for: .normal)
    }
    
    @IBAction func speakTxText(_ sender: Any) {
        let speechUtterance = AVSpeechUtterance(string: txtextView.text)
        speechUtterance.voice  = AVSpeechSynthesisVoice(language: "en-GB")
        speechSynthesizer.speak(speechUtterance)
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayback)
            try audioSession.setActive(false, with: .notifyOthersOnDeactivation)
        } catch {
             print("Audio Session Error")
        }
    }
    
    @IBAction func pauseSpeech(_ sender: Any) {
        //pauseButton.setBackgroundImage(UIImage(named: "pause1"), for: .normal)
        //playButton.setBackgroundImage(UIImage(named: "play0"), for: .normal)
        //stopButton.setBackgroundImage(UIImage(named: "stop0"), for: .normal)
        speechSynthesizer.pauseSpeaking(at: AVSpeechBoundary.word)
    }
    
    @IBAction func stopSpeech(_ sender: Any) {
        //pauseButton.setBackgroundImage(UIImage(named: "pause0"), for: .normal)
        //playButton.setBackgroundImage(UIImage(named: "play0"), for: .normal)
        //stopButton.setBackgroundImage(UIImage(named: "stop1"), for: .normal)
        speechSynthesizer.stopSpeaking(at: AVSpeechBoundary.immediate)
    }
    
    @IBAction func recordTapped(_ sender: Any) {
        if audioEngine.isRunning {
            audioEngine.stop()
            //recordButton.setBackgroundImage(UIImage(named: "record0"), for: .normal)
            recognitionRequest?.endAudio()
            //recordButton.isEnabled = false
            //recordButton.setTitle("Start Recording", for: .normal)
        } else {
            startRecording()
            //recordButton.setBackgroundImage(UIImage(named: "record1"), for: .normal)
            //recordButton.setTitle("Stop Recording", for: .normal)
        }
    }
    
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))
    
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.rxtextView.layer.cornerRadius = 5
        self.txtextView.layer.cornerRadius = 5
        self.northView.layer.cornerRadius = 5
        self.southView.layer.cornerRadius = 5
        
        //To close keyboard after writing is finished
        self.hideKeyboardWhenTappedAround()
        
        //Listen for dataService notifications
        //Data service char listener
        NotificationCenter.default.addObserver(self, selector: #selector(self.dataServiceChar), name: NSNotification.Name(rawValue: "dataServiceChar"), object: nil)
        
        
        //Notification listener
        NotificationCenter.default.addObserver(self, selector: #selector(self.didUpdateValue), name: NSNotification.Name(rawValue: "didUpdateValue"), object: nil)
        
        
        stateChanged()
        
        recordButton.isEnabled = false
        speechRecognizer?.delegate = self
        
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            var isButtonEnabled = false
            
            switch authStatus {  //5
            case .authorized:
                isButtonEnabled = true
                
            case .denied:
                isButtonEnabled = false
                print("User denied access to speech recognition")
                
            case .restricted:
                isButtonEnabled = false
                print("Speech recognition restricted on this device")
                
            case .notDetermined:
                isButtonEnabled = false
                print("Speech recognition not yet authorized")
            }
            
            OperationQueue.main.addOperation() {
                self.recordButton.isEnabled = isButtonEnabled
            }
        }

        // Do any additional setup after loading the view.
    }
    
    var dataTransmission: Bool = false
    var textMessage: Bool = false
    var transmittedData: [UInt8] = []
    
    @objc func didUpdateValue(notif: NSNotification) {
        print("NEW NOTIF:")
        incomingChar = notif.object as? CBCharacteristic
        print(incomingChar!.value!)
        
        let incomingData: Data = incomingChar!.value!
    
        let array = [UInt8](incomingData)
        print(array[0])
        
        if(array[0] == 1){
            self.state = errorState.initial
            stateChanged()
            print("Incoming OBD Message")
            dataTransmission = true
            textMessage = false
        }
            
        else if (array[0] == 4)
        {
            print("OBD Message Transmission Terminated")
            dataTransmission = false
            let newState = chooseState(value: transmittedData[0])
            self.state = newState
            if (newState == errorState.speed) {
                mySpeed = Int(transmittedData[0]) - 10
            }
            transmittedData.removeAll() //Clear old data
            stateChanged()
        }
            
        else if(array[0]==2){
            print("Incoming Text Message")
            dataTransmission = true
            textMessage = true
            
        }else if(array[0]==3){
            print("Text Message Transmission Terminated")
            dataTransmission=false
            print("Converting UInt8 array to String")
            //Convert UInt8 to Data
            let data = Data(bytes: transmittedData)
            
            if let newText = String(data: data, encoding: .utf8) {
                print(newText)
                
                let speechUtterance = AVSpeechUtterance(string: newText)
                speechUtterance.voice  = AVSpeechSynthesisVoice(language: "en-GB")
                speechSynthesizer.speak(speechUtterance)
                let audioSession = AVAudioSession.sharedInstance()
                do {
                    try audioSession.setCategory(AVAudioSessionCategoryPlayback)
                    try audioSession.setActive(false, with: .notifyOthersOnDeactivation)
                } catch {
                    print("Audio Session Error")
                }
                
            } else {
                print("not a valid UTF-8 sequence")
            }
            transmittedData.removeAll() //Clear old data
            
        }
        
        if(dataTransmission){
            transmittedData.append(array[0])
        }
        
        
        //let array : [UInt8] = [0, 0, 0, incomingChar!.value!]
        //let data = Data(bytes: array)
        //let value = UInt32(bigEndian: data.withUnsafeBytes { $0.pointee })
     //   print(value)
    }
    
    @objc func dataServiceChar(notif: NSNotification) {
        dataService = notif.object as? CBCharacteristic
        //Test
        //writeData(value: 9)
    }

    func startRecording() {
        
        if recognitionTask != nil {
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSessionCategoryRecord)
            try audioSession.setMode(AVAudioSessionModeMeasurement)
            try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        let inputNode = audioEngine.inputNode
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        
        recognitionRequest.shouldReportPartialResults = true
        
        recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
            
            var isFinal = false
            
            if result != nil {
                
                self.txtextView.text = result?.bestTranscription.formattedString
                isFinal = (result?.isFinal)!
            }
            
            if error != nil || isFinal {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
                self.recordButton.isEnabled = true
            }
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        
        do {
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
        
        txtextView.text = "Say something, I'm listening!"
        
    }
    
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            recordButton.isEnabled = true
        } else {
            recordButton.isEnabled = false
        }
    }
    
    func chooseState(value: UInt8) -> errorState{
        switch value {
        case 5:
            return errorState.battery
        case 6:
            return errorState.temperature
        case 7:
            return errorState.engine
        case 8:
            return errorState.fuel
        case 9:
            return errorState.traction
        default:
            return errorState.speed
        }
    }
    
    func stateChanged(){
        switch self.state {
        case .initial:
            self.alertButton.image = UIImage(named: "traffic")
            self.alertTextButton.text = "Drive Responsibly"
        case .battery:
            self.alertButton.image = UIImage(named: "battery")
            self.alertTextButton.text = "Battery/Charging Error"
        case .temperature:
            self.alertButton.image = UIImage(named: "radiator")
            self.alertTextButton.text = "Coolant Temperature Error"
        case .engine:
            self.alertButton.image = UIImage(named: "engine")
            self.alertTextButton.text = "Check Engine"
        case .fuel:
            self.alertButton.image = UIImage(named: "fuel")
            self.alertTextButton.text = "Low Fuel Error"
        case .traction:
            self.alertButton.image = UIImage(named: "wheel")
            self.alertTextButton.text = "Traction Control Error"
        case . speed:
            self.alertButton.image = UIImage(named: "speed")
            self.alertTextButton.text = "Current Speed: \(self.mySpeed) km/s"
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "settingsSegue") {
            let vc = segue.destination as! SettingsViewController
            vc.blePeripheral = blePeripheral
            vc.dataService = dataService
        }
    }
}

//To convert text to ascii array
extension String {
    var asciiArray: [UInt32] {
        return unicodeScalars.filter{$0.isASCII}.map{$0.value}
    }
}
